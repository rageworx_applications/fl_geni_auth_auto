# FLTK GENI-AUTO
* A FLTK windows application for auto-login for GENI NAC AUTH.

## Required to build source.
1. MSYS2 + MinGW-W64 ( included Windows SDK. )
1. Lastest version of fl_imgtk (debug & sse3(openmp option) )
1. Latest version of fltk-1.4.0.9-custom.

## External licenses
1. FLTK project license
1. fl_imgtk
1. minIni ( see in extlinces directory )
