#ifndef NOCPUID
#include <cpuid.h>
#endif /// of NOCPUID

#define APP_VERSION                 0,9,10,80
#define APP_VERSION_STR             "0.9.10.80"

#define RES_FILEDESC                "FL-GNAUTH-AUTO"

#define IDC_ICON_A                  101
#define IDC_ICON_T                  102

