#ifdef _MSC_VER
    #include <windows.h>
    #pragma warning(disable : 4996)
#else
    #ifdef _WIN32
        #include <windows.h>
    #endif
    #include <sys/types.h>
    #include <unistd.h>
#endif

#include <ctime>
#include <cstdarg>
#include <cstring>

#include "devlogger.h"

#ifdef _MSC_VER
    #define vsnwprintf  _vsnwprintf
    #define snwprintf   _snwprintf
#endif

#define MAX_LOG_FILELENGTH      (10 * 1024 * 1024)
#define MAX_LOG_STRING_LENGTH   (1024)

///////////////////////////////////////////////////////////////////////////

static char     logger_writepath_a[512] = {0};
#ifdef _WIN32
static wchar_t  logger_writepath_w[512] = {0};
#endif

///////////////////////////////////////////////////////////////////////////

#ifdef _WIN32
const char* S2MBSC( const wchar_t* str )
{
    if ( str == NULL )
        return NULL;

    char* pStr = NULL;

    int strSize = WideCharToMultiByte(CP_ACP, 0,str,-1, NULL, 0,NULL, 0);
    pStr = new char[strSize];
    if ( pStr != NULL )
    {
        WideCharToMultiByte(CP_ACP, 0, str, -1, pStr, strSize, 0,0);
    }

    return pStr;
}

const wchar_t* S2WCS( const char* str )
{
    if ( str == NULL )
        return NULL;

    wchar_t* pStr = NULL;

    int strSize = MultiByteToWideChar(CP_ACP, 0,str, -1, NULL, 0);
    pStr = new WCHAR[strSize];
    if ( pStr != NULL )
    {
        MultiByteToWideChar(CP_ACP, 0,str, strlen(str)+1, pStr, strSize);
    }

    return pStr;
}
#endif /// of _WIN32

///////////////////////////////////////////////////////////////////////////

void DevLogger::SetDefaultWritePath( const char* path )
{
    if ( path != NULL )
    {
        snprintf( logger_writepath_a, 512, "%s", path );

        // check last seperator.
        size_t slen = strlen( logger_writepath_a );
        if ( ( logger_writepath_a[slen-1] == '\\' )
             || ( logger_writepath_a[slen-1] == '/' ) )
        {
            logger_writepath_a[slen-1] = 0x00;
        }
#ifdef _WIN32
#ifdef _MSC_VER
        snwprintf( logger_writepath_w, 512, L"%s", S2WCS(logger_writepath_a) );
#else
        snwprintf( logger_writepath_w, 512, L"%S", S2WCS(logger_writepath_a) );
#endif
#endif
    }
}

const char* DevLogger::DefaultWritePathA()
{
    return logger_writepath_a;
}

#ifdef _WIN32
void DevLogger::SetDefaultWritePath( const wchar_t* path )
{
    if ( path != NULL )
    {
#ifdef _MSC_VER
        snwprintf( logger_writepath_w, 512, L"%s", path );
#else
        snwprintf( logger_writepath_w, 512, L"%S", path );
#endif

        // check last seperator.
        size_t slen = wcslen( logger_writepath_w );
        if ( ( logger_writepath_w[slen-1] == L'\\' )
             || ( logger_writepath_w[slen-1] == L'/' ) )
        {
            logger_writepath_w[slen-1] = 0x0000;
        }

        snprintf( logger_writepath_a, 512, "%s", S2MBSC(logger_writepath_w) );
    }
}

const wchar_t* DevLogger::DefaultWritePathW()
{
    return logger_writepath_w;
}
#endif /// of _WIN32


///////////////////////////////////////////////////////////////////////////

DevLogger::DevLogger( const char* logfile, bool doprint )
  : pFile( NULL ),
    printing( doprint ),
    dbglevel( 0 )
{
    prepareenv();

    char logger_fn[256] = {0};

    if ( logfile != NULL )
    {
        if ( strlen( logger_writepath_a ) > 0 )
        {
            snprintf( logger_fn, 256, "%s\\%s", logger_writepath_a, logfile );
        }
        else
        {
            snprintf( logger_fn, 256, "%s", logfile );
        }
    }
#ifndef _WIN32
    else
    {
        snprintf( logger_fn, 256, "%spid_%u.log",
                  logger_writepath_a, getpid() );
    }
#endif /// of _WIN32

    if ( strlen( logger_fn ) > 0 )
    {
        pFile = fopen( logger_fn, "w" );
    }

#ifdef DEBUG
    printf( "DevLogger, file = %s, fp = %X\n", logger_fn, pFile );
#endif
}

#ifdef _WIN32
DevLogger::DevLogger( const wchar_t* logfile, bool doprint )
  : pFile( NULL ),
    printing( doprint ),
    dbglevel( 0 )
{
    prepareenv();

    wchar_t logger_fn[256] = {0};

    if ( logfile != NULL )
    {
        if ( wcslen( logger_writepath_w ) > 0 )
        {
#ifdef _MSC_VER
            snwprintf( logger_fn, 256, L"%s\\%s", logger_writepath_w, logfile );
#else
            snwprintf( logger_fn, 256, L"%S\\%S", logger_writepath_w, logfile );
#endif
        }
        else
        {
#ifdef _MSC_VER
            snwprintf( logger_fn, 256, L"%s", logfile );
#else
            snwprintf( logger_fn, 256, L"%S", logfile );
#endif
        }
    }

    if ( wcslen( logger_fn ) > 0 )
    {
        pFile = _wfopen( logger_fn, L"w" );
    }

#ifdef DEBUG
#ifdef _MSC_VER
    wprintf( L"DevLogger, file = %s, fp = %X\n", logger_fn, pFile );
#else
    wprintf( L"DevLogger, file = %S, fp = %X\n", logger_fn, pFile );
#endif
#endif
}
#endif /// of _WIN32

DevLogger::~DevLogger()
{
    if ( pFile != NULL )
    {
        fclose( pFile );
        pFile = NULL;
    }
}

void DevLogger::Printing( bool onoff )
{
    printing = onoff;
}

bool DevLogger::Printing()
{
    return printing;
}

void DevLogger::DebugLevel( unsigned lvl )
{
    dbglevel = lvl;
}

unsigned DevLogger::DebugLevel()
{
    return dbglevel;
}

void DevLogger::Printf( const char* args, ... )
{
    char tmpbuff[ MAX_LOG_STRING_LENGTH ] = {0};
    va_list vdata;
    va_start( vdata, args );
    vsnprintf( tmpbuff, MAX_LOG_STRING_LENGTH, args, vdata );
    va_end( vdata );

    if ( pFile != NULL )
    {
        fputs( tmpbuff, pFile );
        fflush( pFile );
    }

    if ( printing == true )
    {
        fputs( tmpbuff, stdout );
        fflush( stdout );
    }
}

#ifdef _WIN32
void DevLogger::Printf( const wchar_t* args, ... )
{
    wchar_t tmpbuff[ MAX_LOG_STRING_LENGTH ] = {0};
    va_list vdata;
    va_start( vdata, args );
    vsnwprintf( tmpbuff, MAX_LOG_STRING_LENGTH, args, vdata );
    va_end( vdata );

    if ( pFile != NULL )
    {
        fputws( tmpbuff, pFile );
        fflush( pFile );
    }

    if ( printing == true )
    {
        fputws( tmpbuff, stdout );
        fflush( stdout );
    }
}
#endif /// of _WIN32

void DevLogger::prepareenv()
{
#ifndef _WIN32
    // check access
    if ( access( logger_writepath_a, 0 ) != 0 )
    {
        SetDefaultWritePath( getenv("HOME") );
    }
#else
    // find write base path...
    if ( wcslen( logger_writepath_w ) == 0 )
    {
#ifdef _MSC_VER
        snwprintf( logger_writepath_w, 512, L"%s\\Documents", _wgetenv( L"USERPROFILE" ) );
#else
        snwprintf( logger_writepath_w, 512, L"%S\\Documents", _wgetenv( L"USERPROFILE" ) );
#endif /// of _MSC_VER
    }
#endif /// of _WIN32
}

void DevLogger::DebugMsg( unsigned level, const char* args, ... )
{
    if ( level >= dbglevel )
    {
        char tmpbuff[ MAX_LOG_STRING_LENGTH ] = {0};
        va_list vdata;
        va_start( vdata, args );
        vsnprintf( tmpbuff, MAX_LOG_STRING_LENGTH, args, vdata );
        va_end( vdata );

        Printf( "%s", tmpbuff );
    }
}

#ifdef _WIN32
void DevLogger::DebugMsg( unsigned level, const wchar_t* args, ... )
{
    if ( level >= dbglevel )
    {
        wchar_t tmpbuff[ MAX_LOG_STRING_LENGTH ] = {0};
        va_list vdata;
        va_start( vdata, args );
        vsnwprintf( tmpbuff, MAX_LOG_STRING_LENGTH, args, vdata );
        va_end( vdata );

#ifdef _MSC_VER
        Printf( L"%s", tmpbuff );
#else
        Printf( L"%S", tmpbuff );
#endif /// of _MSC_VER
    }
}
#endif /// of _WIN32

bool DevLogger::MakeDateFileName( const char* prefix, char* out, size_t outsz )
{
    if ( outsz < 5 )
        return false;

    char prefstr[128] = "log_";
    if ( prefix != NULL )
    {
        memset( prefstr, 0, 128 );
        strncpy( prefstr, prefix, 128 );
    }

    char mapped[256] = {0};
    time_t curtime = time( NULL );
    tm* curtm = localtime( &curtime );

    snprintf( mapped, 256,
              "%s_%04d-%02d-%02d-%02d-%02d-%02d.log",
              prefstr,
              curtm->tm_year + 1900,
              curtm->tm_mon + 1,
              curtm->tm_mday,
              curtm->tm_hour,
              curtm->tm_min,
              curtm->tm_sec );

    strncpy( out, mapped, outsz );

    return true;
}

#ifdef _WIN32
bool DevLogger::MakeDateFileName( const wchar_t* prefix, wchar_t* out, size_t outsz )
{
    if ( outsz < 5 )
        return false;

    wchar_t prefstr[128] = L"log_";

    if ( prefix != NULL )
    {
        memset( prefstr, 0, 128*sizeof(wchar_t) );
        wcsncpy( prefstr, prefix, 128 );
    }

    wchar_t mapped[256] = {0};
    time_t curtime = time( NULL );
    tm* curtm = localtime( &curtime );

#ifdef _MSC_VER
    snwprintf( mapped, 256,
               L"%s_%04d-%02d-%02d-%02d-%02d-%02d.log",
#else
    snwprintf( mapped, 256,
               L"%S_%04d-%02d-%02d-%02d-%02d-%02d.log",
#endif /// of _MSC_VER
               prefstr,
               curtm->tm_year + 1900,
               curtm->tm_mon + 1,
               curtm->tm_mday,
               curtm->tm_hour,
               curtm->tm_min,
               curtm->tm_sec );

    wcsncpy( out, mapped, outsz );

    return true;
}
#endif /// of _WIN32
