#ifndef __WINTK_H__
#define __WINTK_H__

#include <vector>
typedef int(__stdcall *MSGBOXWAPI)(IN HWND, IN LPCWSTR, IN LPCWSTR, IN UINT, IN WORD, IN DWORD);

HWND findfrontwindow();
HWND findwindow( HWND htarget );
HDC  getWindowDC( HWND htarget );
HWND findWindowFromProcessId( DWORD dwProcessId );
HWND findWindowFromProcess( HANDLE hProcess );
void getChildWindows( HWND hParent, std::vector< HWND >& hwlist );
HWND findByClassname( std::vector< PROCESSENTRY32 > & plist, const wchar_t* clnm );

#endif /// of __WINTK_H__
