#include <windows.h>
#include <tlhelp32.h>
#include <psapi.h>
#include <cstdio>
#include <cstdlib>

#include "wintk.h"

////////////////////////////////////////////////////////////////////////////////

#define MIN_HANDLEVALUE     32

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////

struct EnumData {
    DWORD dwProcessId;
    HWND hWnd;
};

////////////////////////////////////////////////////////////////////////////////

const wchar_t* findclassfromhandle( HWND h )
{
    static wchar_t clsnm[64] = {0};

    return clsnm;
}

HWND findfrontwindow()
{
    return GetForegroundWindow();
}

HWND findwindow( HWND htarget )
{
    const wchar_t* clsnm = findclassfromhandle( htarget );
    if ( clsnm != NULL )
    {
        return FindWindow( clsnm, NULL );
    }

    return NULL;
}

HDC  getwindowDC( HWND htarget )
{
    return GetDC( htarget );
}

BOOL CALLBACK EnumProc( HWND hWnd, LPARAM lParam )
{
    EnumData& ed = *(EnumData*)lParam;
    DWORD dwProcessId = 0x0;

    GetWindowThreadProcessId( hWnd, &dwProcessId );

    if ( ed.dwProcessId == dwProcessId )
    {
        ed.hWnd = hWnd;
        SetLastError( ERROR_SUCCESS );

        return FALSE;
    }

    return TRUE;
}

HWND findWindowFromProcessId( DWORD dwProcessId )
{
    EnumData ed = { dwProcessId };
    if ( !EnumWindows( EnumProc, (LPARAM)&ed ) && ( GetLastError() == ERROR_SUCCESS ) )
    {
        return ed.hWnd;
    }

    return NULL;
}

HWND findWindowFromProcess( HANDLE hProcess )
{
    return findWindowFromProcessId( GetProcessId( hProcess ) );
}

BOOL CALLBACK EnumChildWin( HWND hwnd, LPARAM lParam )
{
    if ( lParam > 0 )
    {
        vector< HWND >* plist = (vector< HWND >*)lParam;

        if ( plist != NULL )
        {
            plist->push_back( hwnd );
        }
    }

    return TRUE;
}

void getChildWindows( HWND hParent, vector< HWND >& hwlist )
{
    if ( EnumChildWindows( hParent, EnumChildWin, LPARAM( &hwlist ) ) == TRUE )
    {
        Sleep( 200 );
    }
}

HWND findByClassname( std::vector< PROCESSENTRY32 > & plist, const wchar_t* clnm )
{
    if ( plist.size() > 0 )
    {
        wchar_t wcn[512] = {0};

        for( size_t cnt=0; cnt<plist.size(); cnt++ )
        {
            HWND hTest = findWindowFromProcessId( plist[cnt].th32ProcessID  );
            if ( hTest != NULL )
            {
                if ( IsWindowVisible( hTest ) == TRUE )
                {
                    int slen = GetClassName( hTest, wcn, 512 );
                    if ( slen > 0 )
                    {
                        wprintf( L"(%S)", wcn );
                        fflush( stdout );

                        // let check clnm and this
                        if ( wcsstr( wcn, clnm ) != NULL )
                        {
                            return hTest;
                        }
                    }
                }
            }
        }
    }

    return NULL;
}
