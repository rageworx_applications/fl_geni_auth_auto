#ifndef __CONFIGLOADER_H__
#define __CONFIGLOADER_H__

class ConfigLoader
{
    public:
        ConfigLoader( const char* argv0 = NULL );
        virtual ~ConfigLoader();

    public:
        bool Load();
        bool Reload();
        bool Save();

    // properties ---
    public:
        void GetWindowPos( int &x, int &y, int &dt );
        void SetWindowPos( int x, int y, int dt );
        int  GetWindowState() { return winstate; }
        void SetWindowState( int v ) { winstate = v; }
        size_t GetUserID( char*& str );
        void SetUserID( const char* str );
        size_t GetUserPW( char*& str );
        void SetUserPW( const char* str );
        bool GetEnabled();
        void SetEnabled( bool flag );
        bool GetDelayedStart();
        void SetDelayedStart( bool flag );
        bool GetRemoveLog();
        void SetRemoveLog( bool flag );
        size_t BasePath( char*& str );
        size_t BasePath( wchar_t*& str );

    protected:
        void clearall();
        void getbasepath();

    protected:
        bool        loaded;
        int         wx;
        int         wy;
        int         dt;
        int         winstate;
        char        userID[128];
        char        userPW[128];
        bool        en_state;
        bool        ds_state;
        bool        rl_state;

    protected:
        const char* pargv0;
        char        basepath[256];
        wchar_t     basepathw[256];
};

#endif /// of __CONFIGLOADER_H__
