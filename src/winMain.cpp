/*
* This project designed for Windows.
**/
#include <windows.h>
#include <unistd.h>
#include <tlhelp32.h>
#include <psapi.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>

#include "winMain.h"
#include "fl_imgtk.h"

#include <FL/platform.H>
#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/fl_ask.H>
#include <omp.h>

#include "resource.h"
#include "dtools.h"
#include "ltools.h"

#include <FL/Fl_BMP_Image.H>
#include <FL/Fl_PNG_Image.H>
#include <FL/Fl_JPEG_Image.H>
#include <FL/Fl_Native_File_Chooser.H>

#include "wintk.h"

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////

#define DEF_APP_NAME            "FLTK G.A.F win64"
#define DEF_WIDGET_FSZ          12

#ifdef __linux__
#define DEF_WIDGET_FNT          FL_HELVETICA
#else
#define DEF_WIDGET_FNT          FL_FREE_FONT
#endif // __linux__

#define DEF_THREAD_WAIT_MS      3000
#define DEF_CHINPUT_DELAY_MS    10
#define DEF_INPUT_DELAY_MS      1000
#define DEF_MISSING_LIMIT       10

#define DEF_WIN_COLOR_BG        0x33333300
#define DEF_WIN_COLOR_FG        0xFFFFFF00

#define APPLY_THEME_INP( _x_ )  _x_->box( FL_THIN_UP_BOX );\
                                _x_->color( fl_darker( window->color() ) );\
                                _x_->labelcolor( window->labelcolor() );\
                                _x_->textcolor( 0xFF663300 );\
                                _x_->textsize( window->labelsize() );\
                                _x_->labelfont( window->labelfont() );\
                                _x_->labelsize( window->labelsize() );\
                                _x_->cursor_color( 0xFFAA9900);\
                                _x_->callback( WMain::WidgetCB, this )
#define APPLY_THEME_BTN( _x_ )  _x_->color( window->color(), \
                                       fl_darker( window->color() ) );\
                                _x_->labelcolor( window->labelcolor() );\
                                _x_->labelfont( window->labelfont() );\
                                _x_->labelsize( window->labelsize() );\
                                _x_->callback( WMain::WidgetCB, this );\
                                _x_->clear_visible_focus()
#define APPLY_THEME_BOX( _x_ )  _x_->color( window->color() );\
                                _x_->labelcolor( window->labelcolor() );\
                                _x_->labelfont( window->labelfont() );\
                                _x_->labelsize( window->labelsize() )
#define WRITESTATUS( _x_ )      laststatus = _x_;\
                                boxLastStatus->label( laststatus.c_str() );\
                                if ( grpMain->active_r() > 0 ) \
                                    boxLastStatus->redraw()

#define APPLY_THEME_BTN2( _x_ )  _x_->color( winUtil->color(), \
                                        fl_darker( winUtil->color() ) );\
                                 _x_->labelcolor( winUtil->labelcolor() );\
                                 _x_->labelfont( winUtil->labelfont() );\
                                 _x_->labelsize( winUtil->labelsize() );\
                                 _x_->callback( WMain::WidgetCB, this );\
                                 _x_->clear_visible_focus()
#define APPLY_THEME_BOX2( _x_ )  _x_->color( winUtil->color() );\
                                 _x_->labelcolor( winUtil->labelcolor() );\
                                 _x_->labelfont( winUtil->labelfont() );\
                                 _x_->labelsize( winUtil->labelsize() )
#define APPLY_THEME_CHOICE( _x_ )   _x_->box( FL_NO_BOX );\
                                    _x_->color( window->color() );\
                                    _x_->labelcolor( window->labelcolor() );\
                                    _x_->labelsize( window->labelsize() );\
                                    _x_->textcolor( 0xFF663300 );\
                                    _x_->textsize( window->labelsize() );\
                                    _x_->textfont( window->labelfont() );\
                                    _x_->when( FL_WHEN_CHANGED );\
                                    _x_->callback( WMain::WidgetCB, this );\
                                    _x_->clear_visible_focus()
#define APPLY_THEME_SLIDER( _x_ ) _x_->align( FL_ALIGN_LEFT );\
                                  _x_->labelfont( window->labelfont() );\
                                  _x_->labelsize( window->labelsize() );\
                                  _x_->labelcolor( window->labelcolor() );\
                                  _x_->color( 0x554433FF, 0x887766FF );\
                                  _x_->when( FL_WHEN_CHANGED );\
                                  _x_->callback( WMain::WidgetCB, this );\
                                  _x_->clear_visible_focus()

////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////

static string resourcebase;

////////////////////////////////////////////////////////////////////////////////

bool getResource( const char* scheme, uchar** buff, unsigned* buffsz )
{
    wchar_t convscheme[80] = {0,};

    fl_utf8towc( scheme, strlen(scheme), convscheme, 80 );

    HRSRC rsrc = FindResource( NULL, convscheme, RT_RCDATA );
    if ( rsrc != NULL )
    {
        *buffsz = SizeofResource( NULL, rsrc );
        if ( *buffsz > 0 )
        {
            HGLOBAL glb = LoadResource( NULL, rsrc );
            if ( glb != NULL )
            {
                void* fb = LockResource( glb );
                if ( fb != NULL )
                {
                    uchar* cpbuff = new uchar[ *buffsz ];
                    if ( cpbuff != NULL )
                    {
                        memcpy( cpbuff, fb, *buffsz );

                        *buff = cpbuff;

                        UnlockResource( glb );
                        return true;
                    }

                    UnlockResource( glb );
                }
            }
        }
    }

    return false;
}

Fl_RGB_Image* createResImage( const char* scheme )
{
    if ( scheme != NULL )
    {
        uchar* buff = NULL;
        unsigned buffsz = 0;

        if ( getResource( scheme, &buff, &buffsz ) == true )
        {
            Fl_RGB_Image* retimg = new Fl_PNG_Image( scheme, buff, buffsz );
            delete[] buff;

            return retimg;
        }
    }

    return NULL;
}

const char* _ftoa( float f )
{
    static char fstr[80] = {0};
    sprintf( fstr, "%.2f", f );
    return fstr;
}

const char* _wc2utf8( const wchar_t* src )
{
    static char retstr[512] = {0};

    memset( retstr, 0, 512 );

    fl_utf8fromwc( retstr, 512, src, wcslen( src ) );

    return retstr;
}

const wchar_t* _utf8_2wc( const char* src )
{
    static wchar_t retstr[512] = {0};

    memset( retstr, 0, 512 * sizeof(wchar_t) );

    fl_utf8towc( src, strlen( src ), retstr, 512 );

    return retstr;
}

static Fl_RGB_Image* to_flrgb( HDC dc, HBITMAP hBmp )
{
    BITMAPINFO bmi = {0};
    bmi.bmiHeader.biSize = sizeof( bmi.bmiHeader );

    //Get info.
    int ret = \
    GetDIBits( dc,
               hBmp,
               0, 0, NULL, &bmi,
               DIB_RGB_COLORS );

    if ( ret == 0 )
        return NULL;

    int w = bmi.bmiHeader.biWidth;
    int h = bmi.bmiHeader.biHeight;

    unsigned char* buf = new unsigned char[ w * h * 4 ];

    if ( buf == NULL )
        return NULL;

    bmi.bmiHeader.biBitCount = 32;
    bmi.bmiHeader.biCompression = BI_RGB;

    ret = \
    GetDIBits( dc,
               hBmp,
               0, h, buf, &bmi,
               DIB_RGB_COLORS );

    if ( ret == 0 )
        return NULL;

    unsigned bufsz = w * h;
    unsigned char* convbuf = new unsigned char[bufsz*3];

    if ( convbuf == NULL )
        return NULL;

    memset( convbuf, 0, bufsz );

    #pragma omp parallel for shared( convbuf, buf )
    for( unsigned cnty=0; cnty<h; cnty++ )
    {
        for( unsigned cntx=0; cntx<w; cntx++ )
        {
            unsigned bmpy = h - cnty -1;
            unsigned rgbq = ( ( cnty * w ) + cntx ) * 3;
            unsigned bmpq = ( ( bmpy * w ) + cntx ) * 4;

            convbuf[ rgbq + 0 ] = buf[ bmpq + 2 ];
            convbuf[ rgbq + 1 ] = buf[ bmpq + 1 ];
            convbuf[ rgbq + 2 ] = buf[ bmpq + 0 ];
        }
    }

    Fl_RGB_Image* pImage = new Fl_RGB_Image( convbuf, w, h, 3 );

    delete[] buf;
    return pImage;
}

////////////////////////////////////////////////////////////////////////////////

WMain::WMain( int argc, char** argv, const char* loc )
 :  _argc( argc ),
    _argv( argv ),
    crossfade_idx( 0 ),
    crossfade_ratio( 0.0f ),
    window( NULL ),
    busyflag( false ),
    keycapflag( false ),
    poputilflag( false ),
    goingquit( false ),
    poputilret( -1 ),
    lastkey( 0 ),
    winvisstate( 1 ),
    cfgLdr( NULL ),
    ptid( 0 ),
    threadalive( false ),
    timereventid( 0 ),
    logger( NULL ),
    logPath( NULL )
{
    getEnvironments();

    size_t logpath_l = 0;

    cfgLdr = new ConfigLoader();
    if ( cfgLdr != NULL )
    {
        cfgLdr->Load();
        logpath_l = cfgLdr->BasePath( logPath );
    }

    wchar_t logfmap[256] = {0};

    if ( logpath_l > 0 )
    {
        DevLogger::SetDefaultWritePath( logPath );
    }

    logger = new DevLogger( L"gen_auto.log", false );

#ifdef DEBUG
    if ( logger != NULL )
    {
        logger->DebugLevel( 1 );
    }
#else
    if ( logger != NULL )
    {
        logger->DebugLevel( 5 );
    }
#endif /// of DEBUG

    // Create widgets...
    createComponents();
    registerPopMenu();
    filldata();

    // load Windows Title Icons --
    hIconWindowLarge = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         64,
                                         64,
                                         LR_SHARED );

    hIconWindowSmall = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         16,
                                         16,
                                         LR_SHARED );

    hIconNotifier = (HICON)LoadImage( fl_display,
                                      MAKEINTRESOURCE( IDC_ICON_T ),
                                      IMAGE_ICON,
                                      16,
                                      16,
                                      LR_SHARED );

    if ( window != NULL )
    {
        window->icons( hIconWindowLarge, hIconWindowSmall );
    }

    // notifier ---
    notifier = new WinNotify( fl_display, fl_xid( window ),
                              hIconNotifier, this );

    if ( logger != NULL)
    {
        logger->Printf( L"%s trace logging started.\n", wintitlestr );
    }
}

WMain::~WMain()
{
    Fl::remove_timeout( WMain::TimerCB, this );

    killThread();

    int dt = Fl::screen_num( window->x(), window->y() );
    cfgLdr->SetWindowPos( window->x(),
                          window->y(),
                          dt );

    // Get Window visible state ...
    cfgLdr->SetWindowState( winvisstate );
    cfgLdr->Save();

    delete cfgLdr;

    if ( logPath != NULL )
    {
        delete[] logPath;
    }

    if ( logger != NULL )
    {
        logger->Printf( L"Logging finished.\n" );
        delete logger;
        logger = NULL;
    }

    if ( imgLogo != NULL )
    {
        boxLogo->deimage();
        fl_imgtk::discard_user_rgb_image( imgLogo );
    }

    if ( notifier != NULL )
    {
        delete notifier;
    }
}

int WMain::Run()
{
    if ( window != NULL )
    {
        timereventid = 0;
        Fl::add_timeout( .001f, WMain::TimerCB, this );
        createThread();
        return Fl::run();
    }

    return 0;
}

void* WMain::PThreadCall()
{
    // search and input !
    const wchar_t findpnm[]  = L"Genian";
    const wchar_t findpexe[] = L"GNAUTH.EXE";

    vector< PROCESSENTRY32 > proclist;

    // Wait a sec.
    Sleep( 1000 );

    if ( logger != NULL )
    {
        logger->Printf( L"Starting watching thread ...\n" );
    }

    while( threadalive == true )
    {
        string strUserID;
        string strUserPW;
        bool autoinput_enabled = false;
        unsigned searching_wait_ms = DEF_THREAD_WAIT_MS;
        unsigned searching_missed  = 0;

        // check enabled ...
        if ( chkEnableAutoFill->value() > 0 )
        {
            if ( ( inpUserID->value() != NULL ) && ( inpUserPW->value() != NULL ) )
            {
                strUserID = inpUserID->value();
                strUserPW = inpUserPW->value();

                if ( ( strUserID.size() > 2 ) && ( strUserPW.size() > 4 ) )
                {
                    autoinput_enabled = true;
                }
                else
                {
                    strUserID.clear();
                    strUserPW.clear();
                }
            }
        }

        if ( autoinput_enabled == false )
        {
            if ( logger != NULL )
            {
                logger->Printf( L"autofill not enabled, skipping ...\n" );
            }
        }
        else
        {
            HWND hGLI = NULL;
#ifdef LEGACY_PROC_FIND
            PROCESSENTRY32 process;
            ZeroMemory(&process, sizeof(process));
            process.dwSize = sizeof(process);

            HANDLE snapshot = CreateToolhelp32Snapshot( TH32CS_SNAPALL, 0 );

            if ( Process32First(snapshot, &process) )
            {
                while( true )
                {
                    if ( process.dwSize > 0 )
                    {
                        HWND hTest = findWindowFromProcessId( process.th32ProcessID  );
                        if ( hTest != NULL )
                        {
                            if ( IsWindowVisible( hTest ) == TRUE )
                            {
                                wchar_t txt[128] = {0};
                                GetWindowText( hTest, txt, 128 );
                                if ( wcslen(txt) > 0 )
                                {
                                    if ( logger != NULL )
                                    {
                                        logger->DebugMsg( 1, L"[%S] ", process.szExeFile );
                                        logger->DebugMsg( 1, L"processID(0x%X/%u), ",
                                                          process.th32ProcessID, process.th32ProcessID );
                                        logger->DebugMsg( 1, L"HWND(0x%X/%u), ", hTest, hTest );
                                        logger->DebugMsg( 1, L"\"%S\"", txt );
                                        logger->DebugMsg( 1, L"\n" );
                                    }

                                    // check EXE name  ...
                                    if ( wcscmp( process.szExeFile, findpexe ) == 0 )
                                    {
                                        proclist.push_back( process );
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    if ( Process32Next(snapshot, &process) == FALSE )
                        break;
                }

                CloseHandle(snapshot);
            }
#else
            // do not use processlist in new version.
#endif /// of LEGACY_PROC_FIND
            if ( proclist.size() == 0 )
            {
                HWND hTestPrev = NULL;
                HWND hTest = NULL;

                if ( logger != NULL )
                {
                    logger->DebugMsg( 1, L"-- proclist.size() == 0 ...\n" );
                    logger->DebugMsg( 1, L"-- finding window #32770, Genian ...\n" );
                }

                while( ( hTest = FindWindowEx( NULL, hTestPrev, L"#32770", L"Genian" ) ) != NULL )
                {
                    // search different way.
                    if( IsWindowVisible( hTest ) == TRUE )
                    {
                        hGLI = hTest;

                        if ( logger != NULL )
                        {
                            logger->Printf( L"Found Genian at 0x%X(%u)\n", hGLI, hGLI );
                        }

                        break;
                    }

                    hTestPrev = hTest;
                }
            }
            else
            {
                if ( logger != NULL )
                {
                    logger->DebugMsg( 1, "Found processes : %u\n", proclist.size() );
                }

                // check last process.
                size_t pQ = proclist.size() - 1;

                hGLI = findWindowFromProcessId( proclist[pQ].th32ProcessID  );

                proclist.clear();
            }

            if ( hGLI != NULL )
            {
                // Process auto login.
                HWND hTestChild = FindWindowEx( hGLI, 0, NULL, NULL );

                if ( logger != NULL )
                {
                    logger->DebugMsg( 1, L" .. first child HWND : 0x%X/%u\n", hTestChild, hTestChild );
                }

                // find child windows ...
                HWND hButton = FindWindowEx( hGLI, 0, L"Button", NULL );
                if ( hButton != NULL )
                {
#ifdef DEBUG
                    wprintf( L"Button found : 0x%X/%u\n", hButton, hButton );
#endif
                    HWND hEditID = FindWindowEx( hGLI, hButton, L"Edit", NULL );
                    if ( hEditID != NULL )
                    {
#ifdef DEBUG
                        wprintf( L"Edit.ID found : 0x%X/%u\n", hEditID, hEditID );
#endif
                        HWND hEditPW = FindWindowEx( hGLI, hEditID, L"Edit", NULL );
                        if ( hEditPW != NULL )
                        {
#ifdef DEBUG
                            wprintf( L"Edit.PW found : 0x%X/%u\n", hEditPW, hEditPW );
#endif
                            SendMessage( hGLI, WM_SETFOCUS, 0, 0 );
                            SendMessage( hEditID, WM_SETFOCUS, 0, 0 );
                            SendMessage( hEditID, WM_CLEAR, 0, 0 );

                            for( size_t cnt=0; cnt<strUserID.size(); cnt++)
                            {
                                SendMessage( hEditID, WM_CHAR, WPARAM(strUserID[cnt]), 0 );
                                Sleep( DEF_CHINPUT_DELAY_MS );
                            }

                            SendMessage( hEditPW, WM_SETFOCUS, 0, 0 );
                            SendMessage( hEditPW, WM_CLEAR, 0, 0 );

                            for( size_t cnt=0; cnt<strUserPW.size(); cnt++)
                            {
                                SendMessage( hEditPW, WM_CHAR, WPARAM(strUserPW[cnt]), 0 );
                                Sleep( DEF_CHINPUT_DELAY_MS );
                            }

                            // wait while window disappeared by clicking button.
                            while( true )
                            {
                                if ( IsWindowVisible( hGLI ) == FALSE )
                                {
                                    break;
                                }

                                if ( searching_missed > 0 )
                                {
                                    searching_missed = 0;
                                }

                                SendMessage( hButton, BM_CLICK, 0, 0 );

                                if ( threadalive == false )
                                    break;

                                if ( logger != NULL )
                                {
                                    logger->Printf( L"Waiting for dialog be closed in %u ms.\n",
                                                    DEF_INPUT_DELAY_MS );
                                }

                                Sleep( DEF_INPUT_DELAY_MS );
                            }
                        }
                    }
                }

                hGLI = NULL;
            }
            else
            if ( logger != NULL )
            {
                logger->DebugMsg( 1, L"Failed to find process ...\n" );
                searching_missed ++;
            }
        }

        // Process find for DLP enforcemen window ...
        // class name starts with L"Afx:00007FF"
        /// was ... L"Afx:00007FF7F8BC0000:0:0000000000010005:00000000001000A9:0000000000000000",

        HWND hAfxMsg = FindWindowEx( NULL, NULL, NULL, NULL );
        while( hAfxMsg != NULL )
        {
            wchar_t wint[512] = {0};
            wchar_t clsn[512] = {0};

            // Window text must be empty.
            if ( ( GetWindowText( hAfxMsg, wint, 512 ) == 0 )
                 && ( GetClassName( hAfxMsg, clsn, 512 ) > 0 ) )
            {
                if ( wcsstr( clsn, L"Afx:00007FF" ) != NULL )
                {
                    SendMessage( hAfxMsg, WM_CLOSE, 0, 0 );
                    SendMessage( hAfxMsg, WM_QUIT, 0, 0 );
                }
            }
            hAfxMsg = FindWindowEx( NULL, hAfxMsg, NULL, NULL );
        }

        if ( threadalive == true )
        {
            if ( logger != NULL )
            {
                logger->DebugMsg( 1, L"Waiting in %u ms.\n", searching_wait_ms );
            }

            Sleep( searching_wait_ms );
        }
    } /// of while( threadalive == true )

    if ( logger != NULL )
    {
        logger->Printf( L"Exiting watching thread ...\n" );
    }

    pthread_exit( NULL );
    return NULL;
}

void WMain::WidgetCall( Fl_Widget* w )
{
    if ( w == window )
    {
        // prevent to abnormal GUI state with timer effect.
        if ( timereventid > 0 )
            return;

        if ( goingquit == false )
            showhideWindow();

        return;
    }

    if ( w == btnSave )
    {
        btnSave->deactivate();
        grpMain->deactivate();
        grpOverlay->show();

        timereventid = 1000;
        Fl::repeat_timeout( 0.001f, WMain::TimerCB, this );
    }

    if ( w == winUtil )
    {
        if ( poputilflag == true )
        {
            poputilflag = false;
            poputilret = -1;
        }

        winUtil->hide();

        return;
    }

    if ( w == btnUtilYes )
    {
        if ( poputilflag == true )
        {
            poputilflag = false;
            poputilret = 0;

            winUtil->hide();
            window->hide();
        }
        else
        {
            winUtil->hide();
        }

        return;
    }

    if ( w == btnUtilNo )
    {
        if ( poputilflag == true )
        {
            poputilflag = false;
            poputilret = 1;
        }

        winUtil->hide();

        return;
    }

    if ( w == popMenu )
    {
        int pmv = popMenu->value();

        switch( pmv )
        {
            case 0: // Hide / show window
                {
                    showhideWindow();
                }
                break;

            case 1: // Reload config
                reloadConfigs();
                break;

            case 2: // Quit.
                goingquit = true;
                window->hide();
                break;
        }

        return;
    }
}

void WMain::TimerCall()
{
    static bool firstrun = true;

    if ( firstrun == true )
    {
        firstrun = false;

        if ( cfgLdr != NULL )
        {
            if ( cfgLdr->GetWindowState() == 0 )
            {
                showhideWindow();
            }
        }
    }

    if ( window->visible_r() == 0 )
    {
        timereventid = 0;
        return;
    }

    if ( timereventid >= 1000 )
    {
        unsigned subid = timereventid - 1000;

        if ( grpOverlay->visible_r() > 0 )
        {
            float brate = (float)subid / 990.f;

            if ( brate > 1.0f )
            {
                brate = 1.0f;
            }

            int   new_h = window->h() - 20;
            int   new_w = ( grpOverlay->w() - 40 ) * brate;

            window->redraw();
            boxOvlEffect->size( new_w, new_h );

            if ( subid == 0 )
            {
                // save progress ..
                if ( cfgLdr != NULL )
                {
                    if( inpUserID->value() != NULL )
                    {
                        cfgLdr->SetUserID( inpUserID->value() );
                    }

                    if ( inpUserPW->value() != NULL )
                    {
                        cfgLdr->SetUserPW( inpUserPW->value() );
                    }

                    if ( chkEnableAutoFill->value() > 0 )
                    {
                        cfgLdr->SetEnabled( true );
                    }
                    else
                    {
                        cfgLdr->SetEnabled( false );
                    }

                    if ( chkEnableDelayedStart->value() > 0 )
                    {
                        cfgLdr->SetDelayedStart( true );
                    }
                    else
                    {
                        cfgLdr->SetDelayedStart( false );
                    }

                    if ( chkEnableRemoveLog->value() > 0 )
                    {
                        cfgLdr->SetRemoveLog( true );
                    }
                    else
                    {
                        cfgLdr->SetRemoveLog( false );
                    }

                    cfgLdr->SetWindowState( winvisstate );
                    cfgLdr->Save();

                    timereventid += 50;
                }
            }

            boxOvlEffect->redraw();

            if ( subid < 1000 )
            {
                timereventid += 50;
                Fl::repeat_timeout( 0.005f, WMain::TimerCB, this );
            }
            else
            {
                timereventid = 0;
                grpOverlay->hide();
                grpMain->activate();
                btnSave->activate();
                window->redraw();
            }
        }
        else
        {
            timereventid = 0;
        }
    }
}

void WMain::OnNotiEvent( unsigned tp, unsigned wp, unsigned lp )
{
    if ( tp > 0 )
    {
        switch( wp )
        {
            case WM_LBUTTONUP:
                {
                    showhideWindow();
                }
                break;

            case WM_RBUTTONUP:
                if ( poputilflag == false )
                {
                    int mx = 0;
                    int my = 0;
                    Fl::get_mouse( mx, my );

                    int sx = 0;
                    int sy = 0;
                    int sw = 0;
                    int sh = 0;
                    Fl::screen_work_area( sx, sy, sw, sh, mx, my);

                    int putx = mx;
                    int puty = my;

                    if ( putx + winUtil->w() > sw )
                        putx = sw - winUtil->w();

                    if ( puty + winUtil->h() > sh )
                        puty = sh - winUtil->h();

                    winUtil->resize( putx, puty,
                                     winUtil->w(),
                                     winUtil->h() );
                    poputilret = -1;
                    poputilflag = true;
                    winUtil->show();
                    ShowWindow( fl_xid( winUtil ), SW_SHOW );
                    SetWindowPos( fl_xid( winUtil ),
                                  HWND_TOPMOST,
                                  0,0,0,0,
                                  SWP_NOMOVE | SWP_NOSIZE );
                    SetFocus( fl_xid( winUtil ) );
                }
                break;
        }
    }
}

void WMain::getEnvironments()
{
    pathHome        = _wgetenv( L"USERPROFILE" );
    pathSystemBase  = _wgetenv( L"SYSTEMROOT" );
    pathUserData    = _wgetenv( L"LOCALAPPDATA" );
    pathUserRoaming = _wgetenv( L"APPDATA" );
}

void WMain::setdefaultwintitle()
{
    memset( wintitlestr, 0, MAX_WINTITLE_LEN );
    sprintf( wintitlestr, "%s v.%s", DEF_APP_NAME, APP_VERSION_STR );
}

void WMain::createComponents()
{
    setdefaultwintitle();

    unsigned ww = 350;
    unsigned wh = 155;

    window = new Fl_Double_Window( ww, wh, wintitlestr );
    if ( window != NULL )
    {
        window->color( DEF_WIN_COLOR_BG );
        window->labelfont( DEF_WIDGET_FNT );
        window->labelsize( DEF_WIDGET_FSZ );
        window->labelcolor( DEF_WIN_COLOR_FG );

        fl_message_size_ = DEF_WIDGET_FSZ;
        fl_message_window_color_ = DEF_WIN_COLOR_BG;
        fl_message_label_color_ = DEF_WIN_COLOR_FG;
        fl_message_button_color_[ 0 ] = DEF_WIN_COLOR_BG;
        fl_message_button_color_[ 1 ] = DEF_WIN_COLOR_BG;
        fl_message_button_label_color_[ 0 ] = DEF_WIN_COLOR_FG;
        fl_message_button_label_color_[ 1 ] = DEF_WIN_COLOR_FG;

        // continue to child components ...
        window->begin();

        grpMain = new Fl_Group( 0, 0, ww, wh );
        if ( grpMain != NULL )
        {
            grpMain->begin();
        }

        // make transparency logo on back.
        imgLogo = createResImage( "simg_icon" );
        if ( imgLogo != NULL )
        {
            int iw = imgLogo->w();
            int ih = imgLogo->h();

            if ( ih > window->h() )
            {
                float dsf = float( ih ) / float( window->h() - 5 );

                iw /= dsf;
                ih /= dsf;

                Fl_RGB_Image* \
                imgPut = fl_imgtk::rescale( imgLogo, iw, ih,
                                            fl_imgtk::BILINEAR );
                if ( imgPut != NULL )
                {
                    fl_imgtk::discard_user_rgb_image( imgLogo );
                    imgLogo = imgPut;
                }
            }

            fl_imgtk::blurredimage_ex( imgLogo, 3 );
            fl_imgtk::applyalpha_ex( imgLogo, 0.3f );

            boxLogo = new Fl_Box( 0, 0, iw, ih );
            if ( boxLogo != NULL )
            {
                boxLogo->box( FL_NO_BOX );
                boxLogo->align( FL_ALIGN_CLIP | FL_ALIGN_CENTER );
                boxLogo->image( imgLogo );
            }
        }
        else
        {
            boxLogo = NULL;
        }

        int putx = 150;
        int puty = 5;
        int puth = 25;
        int putw = 195;

            inpUserID = new Fl_Input( putx, puty, putw, puth, "ID : " );
            if ( inpUserID != NULL )
            {
                APPLY_THEME_INP( inpUserID );
                puty += puth + 5;
            }

            inpUserPW = new Fl_Input( putx, puty, putw, puth, "Password : ");
            if ( inpUserPW != NULL )
            {
                APPLY_THEME_INP( inpUserPW );
                inpUserPW->input_type( FL_SECRET_INPUT );
                puty += puth + 5;
            }

            puth = 15;

            chkEnableAutoFill = new Fl_Check_Button( putx, puty, putw, puth, "Enable auto fill" );
            if ( chkEnableAutoFill != NULL )
            {
                APPLY_THEME_BTN( chkEnableAutoFill );
                chkEnableAutoFill->value( 0 );
                chkEnableAutoFill->tooltip( "Automatically fill ID and password\n"
                                            "when GENAUTH.EXE login window appeared.\n"
                                            "Please check when you completed to fill\n"
                                            "correct ID and password." );
                puty += puth + 5;
            }

            chkEnableDelayedStart = new Fl_Check_Button( putx, puty, putw, puth, "Delayed start up" );
            if ( chkEnableDelayedStart != NULL )
            {
                APPLY_THEME_BTN( chkEnableDelayedStart );
                chkEnableDelayedStart->value( 0 );
                chkEnableDelayedStart->tooltip( "It will wait for Windows login process \n"
                                                "being completed when you registered this\n"
                                                "program at taskschd.msc.\n" );
                puty += puth + 5;
            }

            chkEnableRemoveLog = new Fl_Check_Button( putx, puty, putw, puth, "Auto remove logs" );
            if ( chkEnableRemoveLog != NULL )
            {
                APPLY_THEME_BTN( chkEnableRemoveLog );
                chkEnableRemoveLog->value( 0 );
                chkEnableRemoveLog->tooltip( "It will removes all offensive log files\n"
                                             "in %SYSTEMROOT% by name of 'UGnUpdate*.log'" );
                puty += puth + 5;
            }

            puth = 25;

            btnSave = new Fl_Button( putx, puty, putw, puth, "&Save" );
            if ( btnSave != NULL )
            {
                APPLY_THEME_BTN( btnSave );
            }

        if ( grpMain != NULL )
        {
            grpMain->end();
        }

        grpOverlay = new Fl_Group( 0, 0, ww, wh );
        if ( grpOverlay != NULL )
        {
            grpOverlay->begin();
        }

            boxOvlBg = new Fl_TransBox( 0, 0, ww, wh );
            if ( boxOvlBg != NULL )
            {
                //boxOvlBg->set_alpha( 0x50 );
                boxOvlBg->color( 0x0000007F );
            }

            boxOvlEffect = new Fl_TransBox( 10, 10, ww-20, wh-20 );
            if ( boxOvlEffect != NULL )
            {
                //boxOvlEffect->set_alpha( 0x40 );
                boxOvlEffect->color( 0xFFFFFF8F );
            }

        if ( grpOverlay != NULL )
        {
            grpOverlay->end();
            grpOverlay->hide();
        }

        window->end();
        window->callback( WMain::WidgetCB, this );

        int wx = 0;
        int wy = 0;
        int ww = window->w();
        int wh = window->h();
        int dt = 0;

        cfgLdr->GetWindowPos( wx, wy, dt );

        int maxdt = Fl::screen_count();

        if ( dt > maxdt )
        {
            // seems screen changed.
            dt = maxdt;
            // reset to position 20,20.
            wx = 20;
            wy = 20;
        }

        window->resize( wx, wy, ww, wh );
 	    window->show();
    }

    ww = 300;
    wh = 50;

    winUtil = new Fl_Window( ww, wh, wintitlestr );
    if ( winUtil != NULL )
    {
        winUtil->color( DEF_WIN_COLOR_BG );
        winUtil->labelfont( DEF_WIDGET_FNT );
        winUtil->labelsize( DEF_WIDGET_FSZ );
        winUtil->labelcolor( DEF_WIN_COLOR_FG );
        winUtil->border( 0 );
        winUtil->begin();

        boxUtilBorder = new Fl_Box( 0,0, winUtil->w(), winUtil->h() );
        if ( boxUtilBorder != NULL )
        {
            APPLY_THEME_BOX2( boxUtilBorder );
            boxUtilBorder->box( FL_THIN_UP_BOX );
        }

        boxUtilIcon = new Fl_Box( 0,0, 50, 50, "?" );
        if ( boxUtilIcon != NULL )
        {
            APPLY_THEME_BOX2( boxUtilIcon );

            boxUtilIcon->box( FL_NO_BOX );
            boxUtilIcon->labelsize( boxUtilIcon->labelsize() * 3 );
        }

        boxUtilMsg = new Fl_Box( 55, 0, 245, 20 );
        if ( boxUtilMsg != NULL )
        {
            APPLY_THEME_BOX2( boxUtilMsg );
            static char strUtilMsg[128] = {0};
            snprintf( strUtilMsg, 128, "Quit Geni** NAC auto login ?" );
            boxUtilMsg->label( strUtilMsg );
        }

        btnUtilYes = new Fl_Button( 55, 23, 110, 20 );
        if ( btnUtilYes != NULL )
        {
            APPLY_THEME_BTN2( btnUtilYes );
            btnUtilYes->label( "Yes" );
        }

        btnUtilNo = new Fl_Button( 170, 23, 110, 20 );
        if ( btnUtilNo != NULL )
        {
            APPLY_THEME_BTN2( btnUtilNo );
            btnUtilNo->label( "No" );
        }

        winUtil->end();
        winUtil->callback( WMain::WidgetCB, this );
        winUtil->hide();
    }
}

void WMain::reloadConfigs()
{
    cfgLdr->Save();

    if ( cfgLdr->Reload() == true )
    {
        filldata();
    }
}

void WMain::filldata()
{
    char* tmpUID = NULL;
    if ( cfgLdr->GetUserID( tmpUID ) > 0 )
    {
        inpUserID->value( tmpUID );
        delete[] tmpUID;
    }

    char* tmpUPW = NULL;
    if ( cfgLdr->GetUserPW( tmpUPW ) > 0 )
    {
        inpUserPW->value( tmpUPW );
        delete[] tmpUPW;
    }

    if ( cfgLdr->GetEnabled() == true )
    {
        chkEnableAutoFill->value( 1 );
    }
    else
    {
        chkEnableAutoFill->value( 0 );
    }

    if ( cfgLdr->GetDelayedStart() == true )
    {
        chkEnableDelayedStart->value( 1 );
    }
    else
    {
        chkEnableDelayedStart->value( 0 );
    }

    if ( cfgLdr->GetRemoveLog() == true )
    {
        chkEnableRemoveLog->value( 1 );
    }
    else
    {
        chkEnableRemoveLog->value( 0 );
    }
}

void WMain::showhideWindow()
{
    HWND hME = fl_xid( window );
    LONG wstyle = GetWindowLong( hME, GWL_STYLE );

    // FLTK is WS_EX_TOOLWINDOW, not APPWINDOW.

    if ( ( wstyle & WS_VISIBLE ) == 0 )
    {
        ShowWindow( hME, SW_MINIMIZE );
        wstyle |= WS_VISIBLE;
        SetWindowLong( hME, GWL_STYLE, wstyle );
        ShowWindow( hME, SW_RESTORE );
        SetActiveWindow( hME );
        SetForegroundWindow( hME );
        window->activate();
        window->redraw();
        SetFocus( fl_xid( window ) );
        winvisstate = 1;
   }
    else
    {
        ShowWindow( hME, SW_HIDE );
        wstyle &= ~( WS_VISIBLE );
        SetWindowLong( hME, GWL_STYLE, wstyle );
        ShowWindow( hME, SW_HIDE );
        winvisstate = 0;
    }

    if ( cfgLdr != NULL )
    {
        cfgLdr->SetWindowState( winvisstate );
        cfgLdr->Save();
    }
}

void WMain::registerPopMenu()
{
    popMenu = new Fl_Menu_Button( 0, 0, window->w(), window->h() );
    if ( popMenu != NULL )
    {
        popMenu->type( Fl_Menu_Button::POPUP3 );
        popMenu->color( window->color(), window->labelcolor() );
        popMenu->selection_color( window->color() );
        popMenu->labelfont( window->labelfont() );
        popMenu->textfont( window->labelfont() );
        popMenu->labelsize( window->labelsize() );
        popMenu->textsize( window->labelsize() );
        popMenu->labelcolor( window->labelcolor() );
        popMenu->textcolor( window->labelcolor() );

        int ckey[] = {
            FL_CTRL + 'h',
            FL_CTRL + 'r',
            FL_CTRL + 'q',
            0x20 };

        popMenu->add( "Show or Hide\t", ckey[0], WMain::WidgetCB, this,
                      0 );
        popMenu->add( "Reload      \t", ckey[1], WMain::WidgetCB, this,
                      FL_MENU_DIVIDER);
        popMenu->add( "Quit        \t", ckey[2], WMain::WidgetCB, this,
                      0 );

        // Ok. then, insert popup to main window.
        window->add( popMenu );
    }
}

bool WMain::createThread()
{
    if ( ptid == 0 )
    {
        threadalive = true;

        int ptret = pthread_create( &ptid,
                                    NULL,
                                    WMain::PThreadCB,
                                    this );
        if ( ptret == 0 )
            return true;

        threadalive = false;
    }

    return false;
}

void WMain::killThread()
{
    // godam windows only sens it is warning.
    if ( ( ptid != 0 ) && ( threadalive == true ) )
    {
        threadalive = false;
        pthread_kill( ptid, 0 );
        pthread_join( ptid, NULL );
    }
}
