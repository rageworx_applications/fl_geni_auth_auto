#include <unistd.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>

#include <FL/fl_utf8.h>
#include <minIni.h>
#include "cfgldr.h"

///////////////////////////////////////////////////////////////////////

using namespace std;

///////////////////////////////////////////////////////////////////////

#define CFGF_NAME   "flgenauto.cfg"
#define DEFAULT_INI "[WINDOW]\n"\
                    "X=20\n"\
                    "Y=20\n"\
                    "WM=0\n"\
                    "DT=0\n"\
                    "WS=1\n"\
                    "[USER]\n"\
                    "id=\n"\
                    "pw=\n"\
                    "en=0\n"\
                    "ds=1\n"\
                    "rl=0\n"

///////////////////////////////////////////////////////////////////////

void createdefaultini( string &fpath )
{
    FILE* fp = fopen( fpath.c_str(), "wb" );
    if ( fp != NULL )
    {
        char refstr[] = DEFAULT_INI;
        fwrite( refstr, 1, strlen( refstr ), fp );
        fclose( fp );
    }
    else
    {
        printf( "Failed to create INI !\n" );
    }
}

///////////////////////////////////////////////////////////////////////

ConfigLoader::ConfigLoader( const char* argv0 )
  : pargv0( argv0 ),
    en_state( false ),
    ds_state( true )
{
    clearall();
    getbasepath();
}

ConfigLoader::~ConfigLoader()
{
    Save();
}

bool ConfigLoader::Load()
{
    loaded = false;

    string loadpath = basepath;

    if ( loadpath.size() == 0 )
    {
        loadpath = ".";
    }

    // check directory first.
    if ( access( loadpath.c_str(), 0 ) != 0 )
    {
#if defined(_WIN32)
        mkdir( loadpath.c_str() );
#else
        mkdir( loadpath.c_str(), 0775 );
#endif
    }

#if defined(_WIN32)
    loadpath += "\\";
#else
    loadpath += "/";
#endif /// of defined(_WIN32)
    loadpath += CFGF_NAME;

    if ( access( loadpath.c_str(), 0 ) != 0 )
    {
        createdefaultini( loadpath );
    }

#ifdef DEBUG
    printf( "Starting to load config in %s ...\n", loadpath.c_str() );
    fflush( stdout );
#endif
    minIni *pIni = new minIni( loadpath );
    if ( pIni != NULL )
    {
        wx = pIni->geti( "WINDOW", "X", 10 );
        wy = pIni->geti( "WINDOW", "Y", 10 );
        dt = pIni->geti( "WINDOW", "DT", 0 );
        winstate = pIni->geti( "WINDOW", "WS", 1 );

        if ( winstate < 0 )
        {
            winstate = 0;
        }

        string pstr;

        memset( userID, 0, 128 );
        pstr = pIni->gets( "USER", "id", "NOID" );
        if ( pstr.size() > 0 )
        {
            if( pstr != "NOID" )
            {
                strncpy( userID, pstr.c_str(), 128 );
            }
        }

        memset( userPW, 0, 128 );
        pstr = pIni->gets( "USER", "pw", "NOPW" );
        if ( pstr.size() > 0 )
        {
            if ( pstr != "NOPW" )
            {
                strncpy( userPW, pstr.c_str(), 128 );
            }
        }

        en_state = pIni->getbool( "USER", "en", false );
        ds_state = pIni->getbool( "USER", "ds", true );
        rl_state = pIni->getbool( "USER", "rl", false );

        delete pIni;

        loaded = true;
    }

    return loaded;
}

bool ConfigLoader::Reload()
{
    string tmpbp = basepath;
    clearall();
    strcpy( basepath, tmpbp.c_str() );
    return Load();
}

bool ConfigLoader::Save()
{
    if ( loaded == true )
    {
        string loadpath = basepath;

        if ( loadpath.size() == 0 )
        {
            loadpath = ".";
        }

#if defined(_WIN32)
        loadpath += "\\";
#else
        loadpath += "/";
#endif
        loadpath += CFGF_NAME;

        minIni *pIni = new minIni( loadpath );
        if ( pIni != NULL )
        {
            pIni->put( "WINDOW", "X", wx );
            pIni->put( "WINDOW", "Y", wy );
            pIni->put( "WINDOW", "DT", dt );
            pIni->put( "WINDOW", "WS", winstate );

            pIni->put( "USER", "id", userID );
            pIni->put( "USER", "pw", userPW );

            pIni->put( "USER", "en", en_state );
            pIni->put( "USER", "ds", ds_state );
            pIni->put( "USER", "rl", rl_state );

            delete pIni;

            return true;
        }
    }
    return false;
}

void ConfigLoader::GetWindowPos( int &x, int &y,  int &dt )
{
    x = wx;
    y = wy;
    dt = this->dt;
}

void ConfigLoader::SetWindowPos( int x, int y, int dt )
{
    wx = x;
    wy = y;
    this->dt = dt;
}

size_t ConfigLoader::GetUserID( char*& str )
{
    size_t len = strlen(userID);

    if ( len > 0 )
    {
        str = strdup(userID);
    }

    return len;
}

void ConfigLoader::SetUserID( const char* str )
{
    if ( str != NULL )
    {
        snprintf( userID, 128, "%s", str );
    }
}

size_t ConfigLoader::GetUserPW( char*& str )
{
    size_t len = strlen(userPW);

    if ( len > 0 )
    {
        str = strdup(userPW);
    }

    return len;
}

void ConfigLoader::SetUserPW( const char* str )
{
    if ( str != NULL )
    {
        snprintf( userPW, 128, "%s", str );
    }
}

bool ConfigLoader::GetEnabled()
{
    return en_state;
}

void ConfigLoader::SetEnabled( bool flag )
{
    en_state = flag;
}

bool ConfigLoader::GetDelayedStart()
{
    return ds_state;
}

void ConfigLoader::SetDelayedStart( bool flag )
{
    ds_state = flag;
}

bool ConfigLoader::GetRemoveLog()
{
    return rl_state;
}

void ConfigLoader::SetRemoveLog( bool flag )
{
    rl_state = flag;
}

size_t ConfigLoader::BasePath( char*& str )
{
    size_t len = strlen(basepath);

    if ( len > 0 )
    {
        str = strdup(basepath);
    }

    return len;
}

size_t ConfigLoader::BasePath( wchar_t*& str )
{
    size_t len = wcslen(basepathw);

    if ( len > 0 )
    {
        str = wcsdup(basepathw);
    }

    return len;
}

void ConfigLoader::clearall()
{
    memset( userID, 0, 128 );
    memset( userPW, 0, 128 );
    en_state = false;
    ds_state = true;
}

void ConfigLoader::getbasepath()
{
    const wchar_t* ppath = _wgetenv( L"LOCALAPPDATA" );
    if ( ppath != NULL )
    {
        memset( basepathw, 0, 256 * sizeof( wchar_t ) );
        wcsncpy( basepathw, ppath, 256 );

        // convert to damn wchar_t to utf8.
        char convutf8[256] = {0};
        unsigned clen = fl_utf8fromwc( convutf8,
                                       256,
                                       ppath,
                                       wcslen( ppath ) );
        if ( clen > 0 )
        {
            memset( basepath, 0, 256 );
            strcpy( basepath, convutf8 );
            strcat( basepath, "\\FLGENAUTO" );
            wcscat( basepathw, L"\\FLGENAUTO" );
        }
    }
#ifdef DEBUG
    printf( "ConfigLoader, basepath = [%s]\n", basepath );
#endif
}
