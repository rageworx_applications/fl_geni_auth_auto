////////////////////////////////////////////////////////////////////////////////
//
//  devlogger
//  ==========================================================================
//   a very simple C++ class for let logger for developer or anything to trace
//  what error occured while what code belong to.
//
//  tested on :
//      - Windows, MinGW-W64
//      - Windows, M$VC
//      - Linux, x86.64 and aarch64 gcc
//      - MacOSX, llvm-gcc
//
//  (C) 2020 Raphael Kim
//
////////////////////////////////////////////////////////////////////////////////

#ifndef __DEVLOGGER_H__
#define __DEVLOGGER_H__
#ifdef _MSC_VER
#pragma once
#endif /// of _MSC_VER

////////////////////////////////////////////////////////////////////////////////

#include <cstdio>
#include <cstdlib>
#include <cmath>

////////////////////////////////////////////////////////////////////////////////
// --- devlogger version u3
//     0xFF000000 : major
//     0x00FF0000 : minor
//     0x0000FF00 : revision
//     0x000000FF : build of minor
#define DEVLOGGER_VERSION           0x00030008

////////////////////////////////////////////////////////////////////////////////
class DevLogger
{
    public:
        static void SetDefaultWritePath( const char* path );
        static const char* DefaultWritePathA();
        static bool MakeDateFileName( const char* prefix, char* out, size_t outsz );
#ifdef _WIN32
        static void SetDefaultWritePath( const wchar_t* path );
        static const wchar_t* DefaultWritePathW();
        static bool MakeDateFileName( const wchar_t* prefix, wchar_t* out, size_t outsz );
#endif /// of _WIN32

#ifdef _WIN32
    #if defined(UNICODE)||defined(_UNICODE)||defined(_MSC_VER)
        #define DefaultWritePath    DefaultWritePathW
    #else
        #define DefaultWritePath    DefaultWritePathA
    #endif
#else
    #define DefaultWritePath    DefaultWritePathA
#endif

    public:
        DevLogger( const char* logfile = NULL, bool doprint = false );
#ifdef _WIN32
        DevLogger( const wchar_t* logfile = NULL, bool doprint = false );
#endif /// of _WIN32
        ~DevLogger();

    public:
        void Printing( bool onoff = false );
        bool Printing();
        void DebugLevel( unsigned lvl );
        unsigned DebugLevel();
        void Printf( const char* args, ... );
        void DebugMsg( unsigned level, const char* args, ... );
#ifdef _WIN32
        void Printf( const wchar_t* args, ... );
        void DebugMsg( unsigned level, const wchar_t* args, ... );
#endif /// of _WIN32

    protected:
        void prepareenv();

    protected:
        FILE*       pFile;
        bool        printing;
        unsigned    dbglevel;
};

#endif /// of __DEVLOGGER_H__
