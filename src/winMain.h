#ifndef __WINMAIN_H__
#define __WINMAIN_H__

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_RGB_Image.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Menu_.H>
#include <FL/Fl_Menu_Item.H>
#include <FL/Fl_Menu_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Hor_Fill_Slider.H>

#include "Fl_Image_Button.h"
#include "Fl_TransBox.h"

#include <pthread.h>

#include <vector>
#include <string>

#include "winnotify.h"
#include "cfgldr.h"
#include "devlogger.h"

class WMain : public WinNotifyEvent
{
    public:
        WMain( int argc, char** argv, const char* loc );
        ~WMain();

    public:
        int Run();

    public:
        void* PThreadCall();
        static void* PThreadCB( void* p )
        {
            if ( p != NULL )
            {
                WMain* pwm = (WMain*)p;
                return pwm->PThreadCall();
            }
            return NULL;
        }
        void WidgetCall( Fl_Widget* w );
        static void WidgetCB( Fl_Widget* w, void* p )
        {
            if ( p != NULL )
            {
                WMain* pwm = (WMain*)p;
                pwm->WidgetCall( w );
            }
        }
        void TimerCall();
        static void TimerCB( void* p )
        {
            if ( p != NULL )
            {
                WMain* pwm = (WMain*)p;
                return pwm->TimerCall();
            }
        }

    public:
        void OnNotiEvent( unsigned tp, unsigned wp, unsigned lp );

    private:
        void setdefaultwintitle();
        void resetParameters();
        void createComponents();
        void registerPopMenu();
        void getEnvironments();
        void reloadConfigs();
        void filldata();
        void showhideWindow();
        void updateconfigs();
        bool createThread();
        void killThread();

    private:
        int     _argc;
        char**  _argv;

    private:
        float       crossfade_ratio;
        unsigned    crossfade_idx;
        bool        timermutex;
        bool        busyflag;
        bool        keycapflag;
        bool        poputilflag;
        int         poputilret;
        unsigned    lastkey;
        bool        goingquit;
        int         winvisstate;

    protected:
        Fl_Double_Window*   window;
        Fl_Group*           grpMain;
        Fl_Box*             boxLogo;
        Fl_RGB_Image*       imgLogo;
        Fl_Menu_Button*     popMenu;
        Fl_Input*           inpUserID;
        Fl_Input*           inpUserPW;
        Fl_Check_Button*    chkEnableAutoFill;
        Fl_Check_Button*    chkEnableDelayedStart;
        Fl_Check_Button*    chkEnableRemoveLog;
        Fl_Button*          btnSave;
        Fl_Group*           grpOverlay;
        Fl_TransBox*        boxOvlBg;
        Fl_TransBox*        boxOvlEffect;

    protected:
        Fl_Window*          winUtil;
        Fl_Box*             boxUtilBorder;
        Fl_Box*             boxUtilIcon;
        Fl_Box*             boxUtilMsg;
        Fl_Button*          btnUtilYes;
        Fl_Button*          btnUtilNo;

    protected:
        HICON               hIconWindowLarge;
        HICON               hIconWindowSmall;
        HICON               hIconNotifier;

    private:
        #define             MAX_WINTITLE_LEN        512
        char wintitlestr[MAX_WINTITLE_LEN];

    protected:
        std::wstring        pathHome;
        std::wstring        pathSystemBase;
        std::wstring        pathUserData;
        std::wstring        pathUserRoaming;

	protected:
        std::string         laststatus;

    protected:
        WinNotify*          notifier;
        ConfigLoader*       cfgLdr;
        bool                threadalive;
        pthread_t           ptid;
        unsigned            timereventid;
        DevLogger*          logger;
        wchar_t*            logPath;
};

#endif // __WINMAIN_H__
