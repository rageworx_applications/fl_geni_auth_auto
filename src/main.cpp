#include <windows.h>
#include <tlhelp32.h>
#include <psapi.h>

#include <FL/Fl.H>
#include <FL/Fl_Tooltip.H>
#include <FL/fl_ask.H>
#include "fl_imgtk.h"

#include <string>
#include <locale>

#if ( FL_API_VERSION < 10304 )
    #error "Error, FLTK ABI need 1.3.4 or above"
#endif

#if ( FL_IMGTK_VERSION < 303000 )
    #error "Error, Need FL_IMGTK better build ( 0.3.30.x )"
#endif

#include "winMain.h"
#include "wintk.h"
#include "cfgldr.h"

using namespace std;

////////////////////////////////////////////////////////////////////////////////

#define DEF_APP_CLSNAME         "flgenauto"
#define DEF_WAITING_RETRY_MAX   50

////////////////////////////////////////////////////////////////////////////////

HANDLE hMutex = NULL;

const char* convLoc = NULL;
const char* convLng = "Segoe UI";

char* argv_me_path = NULL;
char* argv_me_bin  = NULL;

////////////////////////////////////////////////////////////////////////////////
//
int X_MessageBoxTimeout(HWND hWnd, const TCHAR* sText, const TCHAR* sCaption, UINT uType, DWORD dwMilliseconds)
{
    int retI = 0;

    static MSGBOXWAPI MessageBoxTimeoutW = NULL;

    HMODULE hUser32 = NULL;
    bool    loaded = false;

    if ( MessageBoxTimeoutW == NULL )
    {
        hUser32 = LoadLibraryA( "user32.dll" );
    }

    if ( MessageBoxTimeoutW == NULL )
    {
        if ( hUser32 != NULL )
        {
            MessageBoxTimeoutW = \
                (MSGBOXWAPI)GetProcAddress(hUser32, "MessageBoxTimeoutW");
        }
    }

    if ( MessageBoxTimeoutW != NULL )
    {
        retI = MessageBoxTimeoutW( hWnd,
                                   sText,
                                   sCaption,
                                   uType,
                                   0,
                                   dwMilliseconds);
        if( hUser32 != NULL )
            FreeLibrary( hUser32 );
    }
    else
    {
        retI = MessageBox(hWnd, sText, sCaption, uType);
    }

    return retI;
}

void procAutoLocale()
{
    LANGID currentUIL = GetSystemDefaultLangID();

    switch( currentUIL & 0xFF )
    {
        case LANG_KOREAN:
            convLoc = "ko_KR.utf8";
            convLng = "Malgun Gothic";
            break;

        case LANG_CZECH:
            convLoc = "cs_CZ.utf8";
            break;

        case LANG_DANISH:
            convLoc = "da_DK.utf8";
            break;

        case LANG_DUTCH:
            convLoc = "nl_NL.utf8";
            break;

        case LANG_ESTONIAN:
            convLoc = "et_EE.utf8";
            break;

        case LANG_FRENCH:
            convLoc = "fr_FR.utf8";
            break;

        case LANG_GERMAN:
            convLoc = "de_DE.utf8";
            break;

        case LANG_GREEK:
            convLoc = "el_GR.utf8";
            break;

        case LANG_JAPANESE:
            convLoc = "ja_JP.utf8";
            convLng = "Meiryo UI";
            break;

        case LANG_CHINESE:
            convLoc = "zh_CN.utf8";
            convLng = "MS YaHei UI";
            break;

        case LANG_CHINESE_TRADITIONAL:
            convLoc = "zh_TW.utf8";
            convLng = "MS JhengHei UI";
            break;

        case LANG_ITALIAN:
            convLoc = "it_IT.utf8";
            break;

        case LANG_RUSSIAN:
            convLoc = "ru_RU.utf8";
            break;

        case LANG_SPANISH:
            convLoc = "es_ES.utf8";
            break;

        case LANG_UZBEK: /// May latin.
            convLoc = "uz_UZ.utf8";
            break;

        default:
            convLoc = "C.utf8";
            break;
    }

	setlocale( LC_ALL, convLoc );
}

void parserArgvZero( const char* argv0 )
{
    string argvextractor = argv0;

#ifdef _WIN32
    char splitter[] = "\\";
#else
    char splitter[] = "/";
#endif

    if ( argvextractor.size() > 0 )
    {
        string::size_type lastSplitPos = argvextractor.rfind( splitter );

        string extracted_path = argvextractor.substr(0, lastSplitPos + 1 );
        string extracted_name = argvextractor.substr( lastSplitPos + 1 );

        argv_me_path = strdup( extracted_path.c_str() );
        argv_me_bin  = strdup( extracted_name.c_str() );
    }
}

void presetFLTKenv()
{
    Fl::set_font( FL_FREE_FONT, convLng );
    Fl_Double_Window::default_xclass( DEF_APP_CLSNAME );

#ifdef __linux__
    fl_message_font_ = FL_HELVETICA;
#else
    fl_message_font_ = FL_FREE_FONT;
#endif // __linux__
    fl_message_size_ = 11;

    Fl_Tooltip::color( 0x15151500 ); // was fl_darker( FL_DARK3 )
    Fl_Tooltip::textcolor( 0xFF663300 );
    Fl_Tooltip::size( 12 );
    Fl_Tooltip::font( fl_message_font_ );
    Fl_Tooltip::enable( 1 );

    Fl::scheme( "flat" );
}

bool waitForLaunchingReady()
{
    // Wait for Explorer.EXE and GnAgent.exe found.
    PROCESSENTRY32 process;

    ZeroMemory(&process, sizeof(process));
    process.dwSize = sizeof(process);
    bool foundExplorer = false;
    bool foundGnAgent = false;
    unsigned retrying = 0;

    while( retrying < DEF_WAITING_RETRY_MAX )
    {
        HANDLE snapshot = CreateToolhelp32Snapshot( TH32CS_SNAPALL, 0 );

        if ( Process32First(snapshot, &process) )
        {
            while( true )
            {
                if ( process.dwSize > 0 )
                {
                    HWND hTest = findWindowFromProcessId( process.th32ProcessID  );
                    if ( hTest != NULL )
                    {
                        if ( foundExplorer == false )
                        {
                            if ( wcscmp( process.szExeFile, L"explorer.exe" ) == 0 )
                            {
                                foundExplorer = true;
                            }
                        }

                        if ( foundGnAgent == false )
                        {
                            if ( wcscmp( process.szExeFile, L"GnAgent.exe" ) == 0 )
                            {
                                foundGnAgent = true;
                            }
                        }

                        if ( ( foundExplorer == true ) && ( foundGnAgent == true ) )
                        {
                            CloseHandle(snapshot);
                            return true;
                        }
                    }
                }

                if ( Process32Next(snapshot, &process) == FALSE )
                    break;
            }

            CloseHandle(snapshot);
        }

        Sleep( 2000 );
        retrying++;
    }

    return false;
}

void removeGenLogs()
{
    system( "del %SYSTEMROOT%\\UGnUpdate*.log" );
}

int main (int argc, char ** argv)
{
    // test Mutex.
    hMutex = CreateMutexA( NULL, FALSE, DEF_APP_CLSNAME );

    if ( GetLastError() == ERROR_ALREADY_EXISTS )
    {
        CloseHandle( hMutex );

        HWND hFound = FindWindowA( DEF_APP_CLSNAME, NULL );

        if ( hFound != NULL )
        {
            DWORD proc_tid = GetWindowThreadProcessId( hFound, NULL );
            DWORD proc_cur = GetCurrentThreadId();
            if ( proc_tid != proc_cur )
            {
                if ( AttachThreadInput( proc_cur, proc_tid, TRUE ) > 0 )
                {
                    BringWindowToTop( hFound );
                    AttachThreadInput( proc_cur, proc_tid, FALSE );
                }
            }
        }

        return 0;
    }

    bool isdelayedstart = false;
    bool isremovelogs   = false;

    ConfigLoader* cfgLdr = new ConfigLoader();
    if ( cfgLdr != NULL )
    {
        cfgLdr->Load();

        isdelayedstart = cfgLdr->GetDelayedStart();
        isremovelogs   = cfgLdr->GetRemoveLog();

        delete cfgLdr;
        cfgLdr = NULL;
    }

    if ( isdelayedstart == true  )
    {
        // wait for Windows Login and GnAgent loaded.
        if ( waitForLaunchingReady() == false )
        {
            X_MessageBoxTimeout( 
                        NULL,
                        L"Cannot detect GnAgent loaded to system.",
                        L"Start up failure",
                        MB_ICONERROR,
                        1000
            );
            return 0;
        }
    }

    int reti = 0;

    parserArgvZero( argv[0] );
    presetFLTKenv();

    Fl::lock();

    procAutoLocale();

    if ( isremovelogs == true )
    {
        removeGenLogs();
    }

    WMain* pWMain = new WMain( argc, argv, convLoc );

    if ( pWMain != NULL )
    {
        reti = pWMain->Run();

        delete pWMain;
    }

    if ( argv_me_path != NULL )
    {
        free( argv_me_path );
    }

    if ( argv_me_bin != NULL )
    {
        free( argv_me_bin );
    }

    ReleaseMutex( hMutex );

    return reti;
}
