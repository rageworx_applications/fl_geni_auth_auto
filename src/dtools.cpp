#include <windows.h>
#include <unistd.h>

#include "dtools.h"

#include <io.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <iostream>
#include <fstream>
#include <ctime>
#include <algorithm>
#include <iterator>

#include <dirent.h>

#include "ltools.h"

////////////////////////////////////////////////////////////////////////////////

#define DEF_DIR_SEP_W       L"\\"
#define DEF_DIR_SEP_M       "\\"

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////

const wchar_t* platformdirseperator()
{
    return DEF_DIR_SEP_W;
}

bool testDirectory( const wchar_t* path )
{
    if ( path != NULL )
    {
        _WDIR* dir = NULL;

        dir = _wopendir( path );

        if ( dir != NULL )
        {
            _wclosedir( dir );
            return true;
        }
    }

    return false;
}

bool createDirectory( const wchar_t* path )
{
    if ( path != NULL )
    {
        if( _wmkdir( path ) == 0 )
            return true;
    }

    return false;
}

bool copyFile( const wchar_t* src, const wchar_t* dst )
{
    if ( ( src != NULL ) && ( dst != NULL ) )
    {
        if ( ::CopyFile( src, dst, FALSE ) == TRUE )
            return true;
    }

    return false;
}

unsigned searchFiles( const wchar_t* fpath, vector<wstring> &files, const wchar_t* ext, bool recursive )
{
    if ( fpath == NULL )
        return 0;

    _WDIR* dir      = NULL;
    struct \
    _wdirent* dire  = NULL;

    dir = _wopendir( fpath );

    if ( dir != NULL )
    {
        while ( ( dire = _wreaddir( dir ) ) )
        {
            wstring pathasm = dire->d_name;

            if ( ( pathasm != L"." ) && ( pathasm != L".." ) )
            {
                if ( recursive == true )
                {
                    // check current name is a file or directory ...
                    wstring pathtest = fpath;
                    pathtest += DEF_DIR_SEP_W;
                    pathtest += pathasm;

                    _WDIR* dirtest = _wopendir( pathtest.c_str() );
                    if ( dirtest != NULL )
                    {
                        vector<wstring> tmplist;
                        searchFiles( pathtest.c_str(),
                                     tmplist,
                                     ext,
                                     true );

                        if ( tmplist.size() > 0 )
                        {
                            // make sub-directory path before file name ...
                            wstring tmpasm = pathasm;
                            tmpasm += DEF_DIR_SEP_W;

                            for( size_t cnt=0; cnt<tmplist.size(); cnt++ )
                            {
                                wstring tmpitem = tmpasm;
                                wstring fndname = tmplist[ cnt ];
                                tmpitem += fndname;

                                if ( ext != NULL )
                                {
                                    wstring strext = extractFileExtension( fndname.c_str() );
                                    if ( strext == ext )
                                    {
                                        files.push_back( tmpitem );
                                    }
                                }
                                else
                                {
                                    files.push_back( tmpitem );
                                }
                            }
                        }

                        _wclosedir( dirtest );
                    }
                    else
                    {
                        if ( ext != NULL )
                        {
                            wstring strext = extractFileExtension( pathasm.c_str() );
                            if ( strext == ext )
                            {
                                files.push_back( pathasm );
                            }
                        }
                        else
                        {
                            files.push_back( pathasm );
                        }
                    }
                }
                else
                {
                    _WDIR* dirtest = _wopendir( pathasm.c_str() );
                    if ( dirtest == NULL )
                    {
                        if ( ext != NULL )
                        {
                            if ( pathasm.find_last_of( ext ) != wstring::npos )
                            {
                                files.push_back( pathasm );
                            }
                        }
                        else
                        {
                            files.push_back( pathasm );
                        }
                    }
                    else
                    {
                        _wclosedir( dirtest );
                    }
                }

            }
        }

        _wclosedir(dir);
    }

    return (unsigned)files.size();
}

bool removeFilesInDir( const wchar_t* fpath, const wchar_t* ext )
{
    if ( fpath != NULL )
    {
        vector<wstring> files;

        unsigned retsz = searchFiles( fpath, files, ext );
        if ( retsz > 0 )
        {
            unsigned failurecnt = 0;

            #pragma omp parallel for
            for( unsigned cnt=0; cnt<retsz; cnt++ )
            {
                wchar_t tmpnm[512] = {0,};

                wsprintf( tmpnm, L"%S%S%S",
                          fpath,
                          DEF_DIR_SEP_W,
                          files[cnt].c_str() );

                if ( _waccess( tmpnm, F_OK ) == 0 )
                {
                    if ( _wunlink( tmpnm ) != 0 )
                    {
                        failurecnt++;
                    }
                }
            }

            files.clear();

            if ( failurecnt > 0 )
                return false;

            return true;
        }

        // If nothing to erase, just return true.
        printf("(nothing)");
        return true;
    }

    return false;
}

void makeDirPlatformize( wstring &path )
{
    if ( path.size() > 0 )
    {
        const wchar_t* dirSep = DEF_DIR_SEP_W;

        size_t fpos = path.find( dirSep );
        while( fpos != string::npos )
        {
            path.replace( fpos, 1, dirSep );
            fpos = path.find( dirSep, fpos );
        }
    }
}

void removeLastDirSep( std::wstring &path )
{
    if ( path.size() > 1 )
    {
        const wchar_t* ppath = path.c_str();
        const wchar_t  dirsep[] = DEF_DIR_SEP_W;

        if ( ppath[ path.size() - 1 ] == dirsep[0] )
        {
            path.erase( path.size() - 1 );
        }
    }
}

const wchar_t* stripFilePath( const wchar_t* refpath )
{
    if ( refpath == NULL )
        return NULL;

    static wstring fpath;

    fpath = refpath;

    size_t last_path_div_pos = fpath.find_last_of( DEF_DIR_SEP_W );

    if ( last_path_div_pos != string::npos )
    {
        fpath = fpath.substr( 0, last_path_div_pos );
    }

    return fpath.c_str();
}

const wchar_t* stripFileName( const wchar_t* refpath )
{
    if ( refpath == NULL )
        return NULL;

    static wstring fpath;

    fpath = refpath;

    size_t last_path_div_pos = fpath.find_last_of( DEF_DIR_SEP_W );

    if ( last_path_div_pos != string::npos )
    {
        fpath = fpath.substr( last_path_div_pos + 1 );
    }

    return fpath.c_str();
}

const wchar_t* removeFilePath( const wchar_t* file_path )
{
    if ( file_path == NULL )
        return NULL;

    static wstring fname;

    fname = file_path;

    size_t last_path_div_pos = fname.find_last_of( DEF_DIR_SEP_W );

    if ( last_path_div_pos != string::npos )
    {
        fname = fname.substr( last_path_div_pos + 1 );
    }

    return fname.c_str();
}

